import axios from 'axios'

const request = axios.create({
  // 配置请求根路径
  baseURL: 'http://47.107.36.89:8989/api/private/v1'
})
// 请求拦截验证是否有token需要授权的API，必须在请求头中使用Authorization字段提供token令牌
request.interceptors.request.use((config) => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
export default request
