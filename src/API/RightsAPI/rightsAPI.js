import request from '@/plugins/request.js'
// 创建页面时获取权限列表信息
export const getRightsListAPI = () => {
  return request.get('/rights/list')
}
