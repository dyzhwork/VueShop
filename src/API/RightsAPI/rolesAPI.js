import request from '@/plugins/request.js'

// 获取角色信息列表信息
export const getRolesListAPPI = () => {
  return request.get('/roles')
}
// 点击确定按钮添加角色信息列表信息
export const addNewRoleAPI = (params) => {
  return request.post('/roles', params)
}
// 点击编辑按钮修改角色的信息
export const editDialogAPI = (params) => {
  return request.get('/roles/' + params)
}

// export const editFormInfoAPI = (params1, params2) => {
//   return request.put(params1, params2)
// }

// 点击确定按钮,确定更新用户信息
export const editFormInfoAPI = (id, params) => {
  return request.put('/roles/' + id, params)
}
// 点击删除角色按钮删除角色
export const rolesdeleteAPI = (params) => {
  return request.delete('/roles/' + params)
}
// 根据ID删除对应的权限
export const removeRightByIdAPI = (params) => {
  return request.delete(params)
}
// 展示分配权限的对话框
export const showSetRightDialogAPI = () => {
  return request.get('/rights/tree')
}
// 监听分配权限对话框的关闭事件
export const allotRightsAPI = (params1, params2) => {
  return request.post(params1, params2)
}
