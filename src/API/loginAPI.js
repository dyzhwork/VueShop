import request from '@/plugins/request'
// 登录进入主页的API
export const loginAPI = (params) => {
  return request.post('/login', params)
}
