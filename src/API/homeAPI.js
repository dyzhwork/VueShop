import request from '@/plugins/request'

export const getMenusListAPI = () => {
  return request.get('/menus')
}
