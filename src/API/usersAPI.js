import request from '@/plugins/request.js'
// 获取用户列表信息
export const getUsersListAPI = (params) => {
  return request.get('/users', params)
}
// 添加新用户信息
export const addUserInfoAPI = (params) => {
  return request.post('/users', params)
}
// 监听用户状态switch的状态改变

export const userStateChangedAPI = (params) => {
  return request.put(params)
}
// 点击编辑按钮,弹出编辑用户信息对话框
export const editDialogBtnAPI = (params) => {
  return request.get('/users/' + params)
}
// 点击确定按钮,确定更新用户信息
export const editUserInfoAPI = (id, params) => {
  return request.put('/users/' + id, params)
}
// 点击删除按钮,根据用户id删除对应的用户信息
export const removeUserByIdAPI = (params) => {
  return request.delete('/users/' + params)
}
// 点击设置按钮显示分配角色的对话框
export const setRolesAPI = () => {
  return request.get('/roles')
}
// 点击分配角色确定按钮对话框确定设置角色信息
export const saveRolesInfoAPI = (params1, params2) => {
  return request.put(params1, params2)
}
