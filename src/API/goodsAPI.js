import request from '@/plugins/request.js'
// 创建页面时获取权限列表信息
/* export const xxxAPI = (params) => {
  return request.get('/xxx', params)
}
*/

// 根据分页获取对应的商品列表
export const getGoodsListAPI = (params) => {
  return request.get('/goods', params)
}
// 通过Id删除商品
export const removeByIdAPI = (params) => {
  return request.delete('/goods/' + params)
}
// 根据分页获取对应的商品列表
export const getCateListAPI = () => {
  return request.get('/categories')
}
// 点击tab切换添加商品属性
export const tabClickedAPI = (id, params) => {
  return request.get(id, params)
}
// 点击添加商品确定添加
export const addGoodAPI = (params) => {
  return request.post('/goods', params)
}
// 创建页面时获取分类列表数据
export const getCateListsAPI = (params) => {
  return request.get('/categories', params)
}
// 获取父级分类的数据列表
export const getParentCateListAPI = (params) => {
  return request.get('/categories', params)
}
// 点击按钮添加新的分类

export const addCateAPI = (params) => {
  return request.post('/categories', params)
}
// 点击编辑按钮显示编辑对话框
export const showeditCateDialogAPI = (params) => {
  return request.get('/categories/' + params)
}
// 点击编辑对话框确定按钮
export const editCateInfoAPI = (id, params) => {
  return request.put('/categories/' + id, params)
}
// 点击删除按钮删除分类
export const removeCateAPI = (params) => {
  return request.delete('/categories/' + params)
}
// 获取参数的列表数据
export const getParamsDataAPI = (id, params) => {
  return request.get(id, params)
}
// 监听对话框的关闭事件
export const addParamsAPI = (id, params) => {
  return request.post(id, params)
}
// 点击按钮展示修改的对话框
export const showEditDialogAPI = (id, params) => {
  return request.get(id, params)
}
// 点击按钮修改数据信息
export const editParamsAPI = (id, params) => {
  return request.put(id, params)
}
// 点击删除按钮删除参数
export const removeParamsAPI = (params) => {
  return request.delete(params)
}
// Tag标签的删除
export const saveAttrValsAPI = (id, params) => {
  return request.put(id, params)
}
