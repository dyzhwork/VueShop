import request from '@/plugins/request.js'
// 获取订单列表数据
export const getOrderListAPI = (params) => {
  return request.get('/orders', params)
}
// 获取物流进度！
export const showProgressBoxAPI = (params) => {
  return request.get(params)
}
