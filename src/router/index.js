import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import Home from '@/views/Home.vue'
import Welcome from '@/views/Welcome/Welcome.vue'
import Users from '@/views/Users/Users.vue'
import Roles from '@/views/Rights/Roles.vue'
import Rights from '@/views/Rights/Rights.vue'
import Goods from '@/views/Goods/Goods.vue'
import AddGood from '@/views/Goods/AddGood.vue'
import Params from '@/views/Goods/Params.vue'
import Categories from '@/views/Goods/Categories.vue'
import Orders from '@/views/Orders/Orders.vue'
import Reports from '@/views/Reports/Reports.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Login
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    redirect: '/home/welcome',
    component: Home,
    children: [
      { path: 'welcome', component: Welcome },
      { path: 'users', component: Users },
      { path: 'rights', component: Rights },
      { path: 'roles', component: Roles },
      { path: 'goods', component: Goods },
      { path: '/goods/add', component: AddGood },
      { path: 'categories', component: Categories },
      { path: 'params', component: Params },
      { path: 'orders', component: Orders },
      { path: 'reports', component: Reports }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫,防止没有token的情况下访问需要携带token的页面
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  // 从缓存中获取token
  const TokenStr = window.sessionStorage.getItem('token')
  // 如果token不符合,跳转到登录页
  if (!TokenStr) return next('/login')
  next()
})

export default router
